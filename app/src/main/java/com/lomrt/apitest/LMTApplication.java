package com.lomrt.apitest;

import android.app.Application;
import android.content.Context;

public class LMTApplication extends Application
{

    private static Context mContext;
    private static boolean activityVisible;

    public static Context getAppContext()
    {
        return LMTApplication.mContext;
    }

    public static boolean isActivityVisible()
    {
        return activityVisible;
    }

    public static void activityResumed()
    {
        activityVisible = true;
    }

    public static void activityPaused()
    {
        activityVisible = false;
    }

    public void onCreate()
    {


        super.onCreate();

        LMTApplication.mContext = getApplicationContext();
    }


}
