package com.lomrt.apitest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity
{

    private JSONObject mJSONObject;
    private JSONArray mJSONArray;
    private TextView mTextViewName;
    private AsyncHttpClient mAsyncHttpClient = new AsyncHttpClient();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextViewName = (TextView) findViewById(R.id.textview_name);

        RequestParams requestParams = new RequestParams();
        requestParams.setUseJsonStreamer(true);

        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("user_id", 8);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        requestParams.put("User", jsonObject);

//        {
//            "User": {
//            "user_id": 8
//            }
//        }

        mAsyncHttpClient.post(getApplicationContext(), "http://dd0c3d8e.ngrok.io/CjcuCsiePHP_2016/API/User/sign/", null, requestParams, RequestParams.APPLICATION_JSON, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                // Handle resulting parsed JSON response here
                Log.e("onSuccess", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                // Handle resulting parsed JSON response here
                Log.e("onFailure", errorResponse.toString());
            }

        });

        LMTAPIClient.sharedSession().getUserData(8, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                // Handle resulting parsed JSON response here
                Log.e("onSuccess", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                // Handle resulting parsed JSON response here
                Log.e("onFailure", errorResponse.toString());
            }

        });
    }
}
