package com.lomrt.apitest;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;

/**
 * Created by leo on 16/8/23.
 */
public class LMTAPIClient
{

    private static LMTAPIClient SHARED_SESSION_INSTANCE = null;
    private static Context mContext = LMTApplication.getAppContext();
    public final String API_VERSION = "3.0";

    AsyncHttpClient mAsyncHttpClient = new AsyncHttpClient();
    private String mStringBaseUrl = "http://dd0c3d8e.ngrok.io";
    private String mStringSessionToken;
    private String mStringApikey = "4jf93s0d#@nokd%u3$jo4o0ilsd@gfjeiew/=+\uD83D\uDC14";

    private LMTAPIClient()
    {
        mAsyncHttpClient = new AsyncHttpClient();
        mAsyncHttpClient.setThreadPool(Executors.newSingleThreadScheduledExecutor());
    }

    public static LMTAPIClient sharedSession()
    {
        synchronized (mContext)
        {
            if (SHARED_SESSION_INSTANCE == null)
            {
                SHARED_SESSION_INSTANCE = new LMTAPIClient();
            } else
            {
            }
        }
        return SHARED_SESSION_INSTANCE;
    }

    public void getUserData(int user_id, ResponseHandlerInterface responseHandler)
    {

        RequestParams requestParams = new RequestParams();
        requestParams.setUseJsonStreamer(true);

        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("user_id", user_id);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        requestParams.put("User", jsonObject);

        mAsyncHttpClient.post(mContext, mStringBaseUrl + "/CjcuCsiePHP_2016/API/User/sign/", null, requestParams, RequestParams.APPLICATION_JSON, responseHandler);
    }
    public void getUserData2(int user_id, ResponseHandlerInterface responseHandler)
    {

        RequestParams requestParams = new RequestParams();
        requestParams.setUseJsonStreamer(true);

        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put("user_id", user_id);

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        requestParams.put("User", jsonObject);

        mAsyncHttpClient.post(mContext, mStringBaseUrl + "/CjcuCsiePHP_2016/API/User/sign2/", null, requestParams, RequestParams.APPLICATION_JSON, responseHandler);
    }
}
